# Fuchsia SDK

This directory contains a subset of the Fuchsia
[Integrator Development Kit](https://fuchsia.dev/fuchsia-src/development/idk)
(IDK) downloaded from the
[public CIPD artifacts](https://chrome-infra-packages.appspot.com/p/fuchsia/sdk/core).

Currently the only need is for some definitions in the `pkg/` subdirectory, so
to save a few GiB of storage that's all we include here. If necessary this could
be expanded to include the full IDK.

## Updating

Example command to download the `f22` version:

```shell
# Replace "f22" below with the desired CIPD version.
$ echo "fuchsia/sdk/core/linux-amd64 f22" | cipd ensure -ensure-file - -root download/

# Copy out the files we need and delete the rest.
$ mv download/pkg pkg
$ rm -rf download/
```

Getting `zbi-rs` library sources:
```shell
$ git clone -n --filter=tree:0 https://fuchsia.googlesource.com/fuchsia
$ cd fuchsia/
$ git sparse-checkout set src/firmware/lib/zbi-rs
$ git checkout origin/releases/f22
$ mv src/ ../
$ cd ..
$ rm -rf ./fuchsia/
```
